from django.shortcuts import render
from django.views.generic.base import View
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.views import APIView

from cassette.serializers import StationListSerializer
from cassette.models import Station


class HomePage(View):
    def get(self, request):
        return render(request, 'cassette/index.html')


class StationsListView(APIView):
    @csrf_exempt
    def post(self, request):
        stations = Station.objects.order_by('id')
        serializer = StationListSerializer(stations, many=True)
        return Response(serializer.data)
