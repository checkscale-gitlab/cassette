
// Radio Player object
const Player = function(stations) {
    this.stations = stations;
    this.isPlaying = false;
    this.index = getStationIndex('classic', this.stations);
    this.vol = 0.5;
}

// Radio Player methods
Player.prototype = {
    play: function (index) {
        var sound;
        var data = this.stations[index];
    
        if (data.howl)
            sound = data.howl;
        else
        {
            sound = data.howl = new Howl({
                src: data.url,
                html5: true,
                format: ['ogg', 'mp3', 'acc'],
            });
        }

        if (sound) {
            sound.volume(this.vol);
            sound.play();
            this.isPlaying = true;
            uiToggleOnPlaying(this.isPlaying);
        }
    },

    stop: function (index) {
        var sound = this.stations[index].howl;

        if (sound) {
            sound.once('stop', () => { sound.fade(sound.volume(), 0, 1000); });
            sound.unload();
            this.isPlaying = false;
            uiToggleOnPlaying(this.isPlaying);
        }
    },

    update: function (index) {
        prev = this.isPlaying;
        this.stop(this.index);
        this.index = index;
        if (prev)
            this.play(this.index);
    },

    volume: function (val) {
        var sound = this.stations[this.index].howl;
        if (sound) {
            this.vol = val;
            sound.volume(this.vol);
        }
    },
};

$(window).on('load', function() {
    var stations = getStations();
    if (stations) 
    {
        var player = new Player(stations);
        
        $('#play-btn').on('click', function () {
            if (!player.isPlaying)
                player.play(player.index);
            else
                player.stop(player.index)
        });

        $('#lofi-btn, #classic-btn, #jazz-btn').on('click', function () {
            if (getStationIndex(this.name, stations) != player.index) {
                i = getStationIndex(this.name, stations)
                player.update(i);
                uiSwitchStation(this.id, stations);
            }
        });

        $('#volume_bar').on('mousedown', function () {
            $('#volume_bar').on('input', function () {
                player.volume(this.value / this.max);
            });
        });


        $('#volume_bar').on('touchstart', function () {
            $('#volume_bar').on('input', function () {
                player.volume(this.value / this.max);
            });
        });

        $('#volume_bar').on('tap', function () {
            player.volume(this.value / this.max);
        });

        $('#volume_bar').on('click', function () {
            player.volume(this.value / this.max);
        });


        $('#volume_mute').on('click', function () {
            Howler.mute((Howler._muted) ? 0 : 1);
        });
    }
    else
    {
        loadStationError();
    }
});

function loadStationError() {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "500",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr["error"]("Не удалось получить список радиостанций.", "Ошибка")
}

function uiToggleOnPlaying(state) {
    if (state) {
        $('#play-btn').removeClass('play');
        $('#play-btn').addClass('pause');
        $('#play-btn').addClass('in_active');

        $('#roller_right, #roller_left').addClass('rotate');
    } else {
        $('#play-btn').removeClass('pause');
        $('#play-btn').removeClass('in_active');
        $('#play-btn').addClass('play');
        $('#roller_right, #roller_left').removeClass('rotate');
    }
}

function uiSwitchStation(selected) {
    // change layout
    $('#lofi-btn, #classic-btn, #jazz-btn').removeClass('in_active');
    $('#' + selected).addClass('in_active');
}

function getStationGenre(id, stations) {
    return stations[id].genre.toLowerCase();
}

function getStationIndex(genre, stations) {
    return stations.map(function (o) {
        return o.genre.toLowerCase()
    }).indexOf(genre);
}

function getStations() {
    var data;
    const csrftoken = $.cookie('csrftoken');
    $.ajax({
        headers: {
            'X-CSRFToken': csrftoken
        },
        type: 'POST',
        url: "/api/getStations",
        dataType: 'json',
        async: false,
        success: function (response) {
            data = response;
        },
        error: function (error) {
            toastr.error(error);
        }
    });
    return data;
}