#!/bin/sh

set -e

python manage.py collectstatic --no-input 
python manage.py migrate

gunicorn website.wsgi:application --bind 0.0.0.0:8000 --daemon